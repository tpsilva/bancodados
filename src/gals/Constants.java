package gals;

public interface Constants extends ScannerConstants, ParserConstants
{
    int EPSILON  = 0;
    int DOLLAR   = 1;

    int t_id = 2;
    int t_numero = 3;
    int t_literal = 4;
    int t_CREATE = 5;
    int t_DATABASE = 6;
    int t_TABLE = 7;
    int t_INSERT = 8;
    int t_INTO = 9;
    int t_VALUES = 10;
    int t_SELECT = 11;
    int t_FROM = 12;
    int t_WHERE = 13;
    int t_AND = 14;
    int t_OR = 15;
    int t_DROP = 16;
    int t_SET = 17;
    int t_ORDER = 18;
    int t_DESC = 19;
    int t_PRIMARY = 20;
    int t_FOREIGN = 21;
    int t_KEY = 22;
    int t_REFERENCES = 23;
    int t_integer = 24;
    int t_varchar = 25;
    int t_char = 26;
    int t_null = 27;
    int t_not = 28;
    int t_is = 29;
    int t_TOKEN_30 = 30; //"="
    int t_TOKEN_31 = 31; //">"
    int t_TOKEN_32 = 32; //"<"
    int t_TOKEN_33 = 33; //">="
    int t_TOKEN_34 = 34; //"<="
    int t_TOKEN_35 = 35; //"<>"
    int t_TOKEN_36 = 36; //"."
    int t_TOKEN_37 = 37; //","
    int t_TOKEN_38 = 38; //"*"
    int t_TOKEN_39 = 39; //";"
    int t_TOKEN_40 = 40; //"`"
    int t_TOKEN_41 = 41; //"("
    int t_TOKEN_42 = 42; //")"

}
