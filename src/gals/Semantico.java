package gals;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Stack;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import models.Attribute;
import models.Attributes;
import models.Index;
import models.Indices;
import models.Restriction;
import models.Restrictions;
import models.ShowSelect;
import models.Tabela;
import models.Table;
import models.Tables;
import view.SemanticObserver;
import view.Window;

public class Semantico implements Constants {

	enum ACTIONS {
		CREATE_TABLE, DROP_TABLE, INSERT, SELECT, CREATE_INDEX, DROP_INDEX
	}

	// create table
	private Table createTable;

	// insert
	private Table insertInto;
	private List<Attribute> ordemColunasInsert = new ArrayList<Attribute>();
	private List<String> valoresInsert = new ArrayList<String>();
	private List<Attribute> atributosInsert = new ArrayList<Attribute>();
	int atributoAtual = 0;

	// select
	boolean selecionarTudo = false;
	private List<Attribute> atributosSelect = new ArrayList<Attribute>();
	private List<Table> tabelasSelect = new ArrayList<Table>();
	private List<Condicao> condicoesSelect = new ArrayList<Condicao>();
	private Condicao condicaoAtual = null;

	private SemanticObserver observer;

	private Stack stack = new Stack();
	private List<Attribute> currentAttributes = new ArrayList<Attribute>();
	private LinkedList insert = new LinkedList();
	private Stack cAttributes = new Stack();
	private String selectedDB;
	private ACTIONS currentAction;

	static String dirSep;
	static String metaFolderPath;
	static String tablesFolderPath;

	private List<Restriction> restrictions = new ArrayList<>();

	private String campoPK;
	private String campoValue;

	private String tableTo;
	private String tableFrom;
	private Object fieldFrom;

	private String errorMessage;

	public Semantico() {
		dirSep = (Window.IS_LINUX ? "/" : "\\");
	}

	public void executeAction(int action, Token token) throws SemanticError, ShowSelect {
		System.out.println("Ação #" + action + ", Token: " + token);

		switch (action) {
		case 0:
			acao0(token);
			break;
		case 1:
			acao1(token);
			break;
		case 2:
			acao2(token);
			break;
		case 3:
			acao3(token);
			break;
		case 4:
			acao4(token);
			break;
		case 5:
			acao5(token);
			break;
		case 6:
			acao6(token);
			break;
		case 7:
			acao7(token);
			break;
		case 8:
			acao8(token);
			break;
		case 9:
			acao9(token);
			break;
		case 10:
			acao10(token);
			break;
		case 11:
			acao11(token);
			break;
		case 12:
			acao12(token);
			break;
		case 13:
			acao13(token);
			break;
		case 14:
			acao14(token);
			break;
		case 15:
			acao15(token);
			break;
		case 16:
			acao16(token);
			break;
		case 17:
			acao17(token);
			break;
		case 18:
			acao18(token);
			break;
		case 19:
			acao19(token);
			break;
		case 20:
			acao20(token);
			break;
		case 21:
			acao21(token);
			break;
		case 22:
			acao22(token);
			break;
		case 23:
			acao23(token);
			break;
		case 24:
			acao24(token);
			break;
		case 25:
			acao25(token);
			break;
		case 26:
			acao26(token);
			break;
		case 27:
			acao27(token);
			break;
		case 28:
			acao28(token);
			break;
		case 29:
			acao29(token);
			break;
		case 30:
			acao30(token);
			break;
		case 31:
			acao31(token);
			break;
		case 32:
			acao32(token);
			break;
		case 33:
			acao33(token);
			break;
		case 34:
			acao34(token);
			break;
		case 35:
			acao35(token);
			break;
		case 36:
			acao36(token);
			break;
		case 37:
			acao37(token);
			break;
		case 40:
			acao40(token);
			break;
		case 41:
			acao41(token);
			break;
		case 42:
			acao42(token);
			break;
		case 43:
			acao43(token);
			break;
		case 44:
			acao44(token);
			break;
		case 45:
			acao45(token);
			break;
		case 46:
			acao46(token);
			break;
		case 47:
			acao47(token);
			break;
		case 48:
			acao48(token);
			break;
		case 49:
			acao49(token);
			break;
		case 50:
			acao50(token);
			break;
		case 51:
			acao51(token);
			break;
		}

	}

	private void acao37(Token token) throws SemanticError {
		try {
			String table = (String) stack.pop();
			ObjectMapper mapper = new ObjectMapper();

			// Remove as Restri��es
			File restricaoFile = new File(metaFolderPath + "restricao.json");
			Restrictions restrictions = mapper.readValue(restricaoFile, Restrictions.class);
			List<Restriction> restrictionsList = restrictions.getRestrictions();
			boolean anyMatch = false;
			try (Stream<Restriction> stream = restrictionsList.stream();) {
				anyMatch = stream.anyMatch((p) -> {
					tableFrom = p.tableFrom;
					fieldFrom = p.attributeFrom;
					return p.tableTo.equalsIgnoreCase(table);
				});
			}
			if (anyMatch) {
				throw new SemanticError(
						"O atributo '" + fieldFrom + "' da tabela '" + tableFrom
								+ "' faz referencia a esta tabela, � necess�rio excluir a referencia(FK) antes de excluir a tabela.",
						token.getPosition());
			}

			restrictionsList.removeIf((r) -> {
				return r.tableFrom.equals(table);
			});
			restrictions.setRestrictions(restrictionsList);
			mapper.writeValue(new File(metaFolderPath + "restricao.json"), restrictions);

			File attribFile = new File(metaFolderPath + "atributo.json");
			Attributes attrib;
			attrib = mapper.readValue(attribFile, Attributes.class);

			// Remove os atributos do metada de atributos
			List<Attribute> attributes = attrib.getAttributes();
			attributes.removeIf((a) -> {
				return a.getIdTabela().equalsIgnoreCase(table);
			});
			attrib.setAttributes(attributes);
			mapper.writeValue(new File(metaFolderPath + "atributo.json"), attrib);

			// Deleta o json da tabela
			File tableFile = new File(tablesFolderPath + table + ".json");
			tableFile.delete();

			// Remove a tabela do metata de tabelas
			File tabelaFile = new File(metaFolderPath + "tabela.json");
			Tables tables = mapper.readValue(tabelaFile, Tables.class);
			List<Table> tablesList = tables.getTables();
			tablesList.removeIf((t) -> {
				return t.getId().equalsIgnoreCase(table);
			});
			tables.setTables(tablesList);
			mapper.writeValue(tabelaFile, tables);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void acao51(Token token) throws SemanticError {
		condicoesSelect.add(condicaoAtual);
		condicaoAtual = null;
	}

	private void acao49(Token token) throws SemanticError {
		condicaoAtual.condicaoNull = true;
		condicaoAtual.isNull = true;
	}

	private void acao50(Token token) throws SemanticError {
		condicaoAtual.isNull = false;
	}

	private void acao48(Token token) throws SemanticError {
		Attribute a = new Attribute();
		a.setId(token.getLexeme());

		if (condicaoAtual == null) {
			Condicao c = new Condicao();
			c.campo1 = a;

			condicaoAtual = c;
		} else {
			condicaoAtual.campo2 = a;

			condicoesSelect.add(condicaoAtual);
			condicaoAtual = null;
		}
	}

	public boolean isNumber(String text) {
		return (text.matches("[0-9]+") && text.length() > 2);
	}

	private void acao36(Token token) throws SemanticError, ShowSelect {
		try {
			ObjectMapper mapper = new ObjectMapper();

			HashMap<String, List<HashMap>> tables = new HashMap();
			for (Table t : tabelasSelect) {
				Tabela ta = mapper.readValue(new File(tablesFolderPath + t.getId() + ".json"), Tabela.class);
				tables.put(t.getId(), ta.getRecords());
			}

			File attribFile = new File(metaFolderPath + "atributo.json");
			Attributes attrib = mapper.readValue(attribFile, Attributes.class);

			for (Condicao c : condicoesSelect) {
				Attribute c1 = c.campo1;
				Attribute c2 = c.campo2;
				Object v1 = c.valor1;
				Object v2 = c.valor2;

				if (c.condicaoNull) {
					if (c.isNull) {
						tables.put((String) c1.getIdTabela(), tables.get(c1.getIdTabela()).stream()
								.filter(g -> g.get(c1.getId()).equals("null")).collect(Collectors.toList()));
					} else {
						tables.put((String) c1.getIdTabela(), tables.get(c1.getIdTabela()).stream()
								.filter(g -> !g.get(c1.getId()).equals("null")).collect(Collectors.toList()));
					}
				} else {

					for (Entry<String, List<HashMap>> e : tables.entrySet()) {

						if ("=".equals(c.operador)) {
							tables.put(e.getKey(),
									e.getValue().stream()
											.filter(g -> (getValue(c1, v1, g)).compareTo(getValue(c2, v2, g)) == 0)
											.collect(Collectors.toList()));
						} else if (">".equals(c.operador)) {
							tables.put(e.getKey(),
									e.getValue().stream()
											.filter(g -> (getValue(c1, v1, g)).compareTo(getValue(c2, v2, g)) > 0)
											.collect(Collectors.toList()));
						} else if (">=".equals(c.operador)) {
							tables.put(e.getKey(),
									e.getValue().stream()
											.filter(g -> (getValue(c1, v1, g)).compareTo(getValue(c2, v2, g)) == 0
													|| (getValue(c1, v1, g)).compareTo(getValue(c2, v2, g)) > 0)
									.collect(Collectors.toList()));
						} else if ("<".equals(c.operador)) {
							tables.put(e.getKey(),
									e.getValue().stream()
											.filter(g -> (getValue(c1, v1, g)).compareTo(getValue(c2, v2, g)) < 0)
											.collect(Collectors.toList()));
						} else if ("<=".equals(c.operador)) {
							tables.put(e.getKey(),
									e.getValue().stream()
											.filter(g -> (getValue(c1, v1, g)).compareTo(getValue(c2, v2, g)) < 0
													|| (getValue(c1, v1, g)).compareTo(getValue(c2, v2, g)) == 0)
									.collect(Collectors.toList()));
						} else if ("<>".equals(c.operador)) {
							tables.put(e.getKey(),
									e.getValue().stream()
											.filter(g -> (getValue(c1, v1, g)).compareTo(getValue(c2, v2, g)) != 0)
											.collect(Collectors.toList()));
						}
					}
				}
			}

			List<HashMap<String, String>> rows = new ArrayList<HashMap<String, String>>();

			for (Table t : tabelasSelect) {
				if (selecionarTudo) {

					List<HashMap> records = tables.get(t.getId());

					for (HashMap record : records) {
						rows.add(record);
					}
				} else {
					List<Attribute> atributosTabela = new ArrayList<>();

					for (Attribute a : atributosSelect) {
						if (a.getIdTabela().equals(t.getId())) {
							atributosTabela.add(a);
						}
					}

					List<HashMap> records = tables.get(t.getId());

					HashMap row = new HashMap();

					for (HashMap record : records) {
						for (Attribute a : atributosTabela) {
							row.put(a.getId(), record.get(a.getId()));
						}
						rows.add(row);
					}
				}
			}

			throw new ShowSelect(rows);

			// List<HashMap> retorno = new ArrayList<HashMap>();
			/*
			 * for (Condicao c : condicoes) { TableField c1 = c.campo1;
			 * TableField c2 = c.campo2; Object v = c.valor2;
			 * 
			 * if ("=".equals(c.operador)) { tables.put((String) c1.tabela,
			 * tables.get(c1.tabela).stream() .filter(g -> ((String)
			 * g.get(c1.atributo)).compareTo(getValue(c2, v, g)) == 0)
			 * .collect(Collectors.toList())); } else if
			 * (">".equals(c.operador)) { tables.put((String) c1.tabela,
			 * tables.get(c1.tabela).stream() .filter(g -> ((String)
			 * g.get(c1.atributo)).compareTo(getValue(c2, v, g)) > 0)
			 * .collect(Collectors.toList())); } else if
			 * (">=".equals(c.operador)) { tables.put((String) c1.tabela,
			 * tables.get(c1.tabela).stream() .filter(g -> ((String)
			 * g.get(c1.atributo)).compareTo(getValue(c2, v, g)) == 0 ||
			 * ((String) g.get(c1.atributo)).compareTo(getValue(c2, v, g)) > 0)
			 * .collect(Collectors.toList())); } else if
			 * ("<".equals(c.operador)) { tables.put((String) c1.tabela,
			 * tables.get(c1.tabela).stream() .filter(g -> ((String)
			 * g.get(c1.atributo)).compareTo(getValue(c2, v, g)) < 0)
			 * .collect(Collectors.toList())); } else if
			 * ("<=".equals(c.operador)) { tables.put((String) c1.tabela,
			 * tables.get(c1.tabela).stream() .filter(g -> ((String)
			 * g.get(c1.atributo)).compareTo(getValue(c2, v, g)) == 0 ||
			 * ((String) g.get(c1.atributo)).compareTo(getValue(c2, v, g)) < 0)
			 * .collect(Collectors.toList())); } else if
			 * ("<>".equals(c.operador)) { tables.put((String) c1.tabela,
			 * tables.get(c1.tabela).stream() .filter(g -> ((String)
			 * g.get(c1.atributo)).compareTo(getValue(c2, v, g)) != 0)
			 * .collect(Collectors.toList())); } }
			 */

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void acao45(Token token) {
		selecionarTudo = true;
	}

	private void acao41(Token token) {
		String tableId = (String) stack.pop();

		Attribute a = new Attribute();
		a.setIdTabela(tableId);
		a.setId(token.getLexeme());

		atributosSelect.add(a);
	}

	private void acao46(Token token) {
		String atributoId = (String) stack.pop();

		Attribute a = new Attribute();
		a.setId(atributoId);
		atributosSelect.add(a);
	}

	private void acao47(Token token) throws SemanticError {
		try {
			if (!selecionarTudo) {
				ObjectMapper mapper = new ObjectMapper();
				File attribFile = new File(metaFolderPath + "atributo.json");
				Attributes attrib = mapper.readValue(attribFile, Attributes.class);

				List<String> tAux = new ArrayList<String>();
				for (Table t : tabelasSelect) {
					tAux.add(t.getId());
				}

				List<Attribute> atributosTabelas = attrib.getAttributes().stream()
						.filter(a -> tAux.contains(a.getIdTabela())).collect(Collectors.toList());

				for (Attribute a : atributosSelect) {

					if (a.getIdTabela() == null) {
						int aux = 0;
						for (Attribute b : atributosTabelas) {
							if (b.getId().equals(a.getId())) {
								if (aux == 1) {
									throw new SemanticError("atributo " + a.getId() + " ambiguo", token.getPosition());
								}
								a.setIdTabela(b.getIdTabela());
								aux++;
							}
						}

						if (aux == 0) {
							throw new SemanticError("atributo " + a.getId() + " nao encontrado", token.getPosition());
						}
					} else {
						boolean aux = false;
						for (Attribute b : atributosTabelas) {
							if (b.getIdTabela().equals(a.getIdTabela())) {
								if (b.getId().equals(a.getId())) {
									aux = true;
								}
							}
						}

						if (!aux) {
							throw new SemanticError("atributo " + a.getIdTabela() + "." + a.getId() + " nao encontrado",
									token.getPosition());
						}
					}

				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void acao35(Token token) throws SemanticError {
		try {

			if (ordemColunasInsert.isEmpty()) {

				ObjectMapper mapper = new ObjectMapper();

				Table t = insertInto;

				HashMap values = new HashMap();

				int aux = 0;

				while (aux < atributosInsert.size()) {
					Attribute coluna = atributosInsert.get(aux);
					String valor = valoresInsert.get(aux);

					if (coluna.isNotNull() && valor == null) {
						throw new SemanticError(
								"Erro - a coluna '" + coluna.getId()
										+ "' � NOT NULL, porem est� sendo inserido um valor nulo.",
								token.getPosition());
					}

					values.put(coluna.getId(), valor);
					aux++;
				}

				// carrega a tabela
				String tabelaPath = tablesFolderPath + t.getId() + ".json";

				Tabela tabela = mapper.readValue(new File(tabelaPath), Tabela.class);

				// Valida se para todos os registros j� inseridos existe algum
				// que tenha a pk com o mesmo valor
				validatePKOnInsert(token, mapper, t, values, tabela);

				// Valida se as FKs que est�o sendo inseridas existem
				validateFKOnInsert(token, mapper, t, values);

				// salva o novo valor
				tabela.addRecord(values);

				mapper.writeValue(new File(tabelaPath), tabela);

			} else {

				ObjectMapper mapper = new ObjectMapper();

				Table t = insertInto;

				HashMap values = new HashMap();

				int aux = 0;

				while (aux < ordemColunasInsert.size()) {
					Attribute coluna = ordemColunasInsert.get(aux);
					String valor = valoresInsert.get(aux);

					if (coluna.isNotNull() && valor == null) {
						throw new SemanticError(
								"Erro - a coluna '" + coluna.getId()
										+ "' � NOT NULL, porem est� sendo inserido um valor nulo.",
								token.getPosition());
					}
					values.put(coluna.getId(), valor);
					aux++;
				}

				// carrega a tabela
				String tabelaPath = tablesFolderPath + t.getId() + ".json";

				Tabela tabela = mapper.readValue(new File(tabelaPath), Tabela.class);

				// Valida se as PKs que est�o sendo inseridas j� n�o existem
				validatePKOnInsert(token, mapper, t, values, tabela);

				// Valida se as FKs que est�o sendo inseridas existem
				validateFKOnInsert(token, mapper, t, values);

				// salva o novo valor
				tabela.addRecord(values);

				mapper.writeValue(new File(tabelaPath), tabela);

			}

		} catch (IOException e) {

		}
	}

	private void validateFKOnInsert(Token token, ObjectMapper mapper, Table t, HashMap values)
			throws IOException, JsonParseException, JsonMappingException, SemanticError {
		File restricaoFile = new File(metaFolderPath + "restricao.json");
		Restrictions restrictions = mapper.readValue(restricaoFile, Restrictions.class);
		List<Restriction> restrictionsList = restrictions.getRestrictions();

		boolean allMatch = false;
		try (Stream<Restriction> stream = restrictionsList.stream().filter((rl) -> {
			return rl.tableFrom.equalsIgnoreCase(t.getId());
		});) {
			allMatch = stream.allMatch((p) -> {
				try {
					File tableFile = new File(tablesFolderPath + p.tableTo + ".json");
					Tabela tabelaFK = mapper.readValue(tableFile, Tabela.class);
					List<HashMap> records = tabelaFK.getRecords();
					errorMessage = "N�o foi encontrado um registro com o valor '" + values.get(p.attributeFrom)
							+ "' na tabela '" + p.tableTo + "', FK Error";

					String attributeTo = p.attributeTo;
					String attributeFrom = p.attributeFrom;
					boolean anyMatch = false;
					try (Stream<HashMap> streamRecords = records.stream();) {
						anyMatch = streamRecords.anyMatch((r) -> {
							return r.get(attributeTo).equals(values.get(attributeFrom));
						});
					}
					return anyMatch;
				} catch (Exception e) {
					e.printStackTrace();
				}

				return false;
			});
		}
		if (!allMatch) {
			throw new SemanticError(errorMessage, token.getPosition());
		}
	}

	private void validatePKOnInsert(Token token, ObjectMapper mapper, Table t, HashMap values, Tabela tabela)
			throws IOException, JsonParseException, JsonMappingException, SemanticError {
		// Valida��o da PK ao inserir
		File attribFile = new File(metaFolderPath + "atributo.json");
		Attributes attrib = mapper.readValue(attribFile, Attributes.class);
		List<Attribute> attributes = attrib.getAttributes();

		boolean anyMatch = false;

		/*
		 * Obtem os campos da tabela que s�o PK Itera sobre todos os registros
		 * da tabela verificando se algum dos campos tem a mesma pk que o
		 * resgistro que est� sendo inserido.
		 */
		try (Stream<HashMap> stream = tabela.getRecords().stream();) {
			anyMatch = stream.anyMatch((a) -> {
				try (Stream<Attribute> attributesStream = attributes.stream().filter((p) -> {
					return p.getIdTabela().equals(t.getId()) && p.isPrimaryKey();
				});) {
					return attributesStream.anyMatch((b) -> {
						campoPK = b.getId();
						campoValue = a.get(b.getId()).toString();
						return a.get(b.getId()).equals(values.get(b.getId()));
					});
				}
			});
		}

		if (anyMatch) {
			throw new SemanticError("j� existe um registro com a PK '" + campoPK + "' e o valor '" + campoValue + "'",
					token.getPosition());
		}
	}

	private void acao43(Token token) {
		insertInto = new Table();
		insertInto.setId(token.getLexeme());
	}

	private void acao44(Token token) {
		Attribute a = new Attribute();
		a.setId(token.getLexeme());
		ordemColunasInsert.add(a);
	}

	private void acao11(Token token) {
		String attribId = (String) stack.pop();

		Attribute attrib = new Attribute();
		attrib.setId(attribId);
		attrib.setType('S');
		attrib.setSize(Integer.valueOf(token.getLexeme()));

		stack.push(attrib);
	}

	private void acao42(Token token) {
		Attribute a = (Attribute) stack.pop();
		a.setNotNull(true);

		stack.push(a);
	}

	private void acao40(Token token) throws SemanticError {
		List<Attribute> atributos = new ArrayList<Attribute>();
		List<String> pks = new ArrayList<String>();

		while (!stack.isEmpty()) {
			Object last = stack.pop();
			if (last instanceof Attribute) {
				atributos.add((Attribute) last);
			} else {
				pks.add((String) last);
			}
		}

		for (String pk : pks) {
			boolean found = false;

			for (Attribute a : atributos) {
				if (a.getId().equals(pk)) {
					a.setPrimaryKey(true);
					found = true;
				}
			}

			if (!found) {
				throw new SemanticError("atributo " + pk + " inexistente", token.getPosition());
			}
		}

		for (Attribute a : atributos) {
			stack.push(a);
		}
	}

	private void acao34(Token token) throws SemanticError {
		try {
			if (createTable != null) {

				ObjectMapper mapper = new ObjectMapper();

				Table currentTable = createTable;

				File tabelaFile = new File(metaFolderPath + "tabela.json");
				Tables tables;

				tables = mapper.readValue(tabelaFile, Tables.class);

				for (Table table : tables.getTables()) {
					if (currentTable.getId().equals(table.getId())) {
						throw new SemanticError("tabela ja existente", token.getPosition());
					}
				}

				tables.getTables().add(currentTable);

				File attribFile = new File(metaFolderPath + "atributo.json");
				Attributes attrib = mapper.readValue(attribFile, Attributes.class);

				List<Attribute> atributos = new ArrayList<Attribute>();
				while (!stack.isEmpty()) {
					atributos.add((Attribute) stack.pop());
				}

				int qtAtrib = atributos.size();

				for (int i = 0; i < atributos.size(); i++) {
					atributos.get(i).setSeq(qtAtrib);
					qtAtrib--;
				}

				for (Attribute a : atributos) {
					a.setIdTabela(currentTable.getId());
					attrib.addAttribute(a);
				}

				File restricaoFile = new File(metaFolderPath + "restricao.json");
				Restrictions restrictions = mapper.readValue(restricaoFile, Restrictions.class);

				restrictions.getRestrictions().addAll(this.restrictions);

				mapper.writeValue(restricaoFile, restrictions);
				mapper.writeValue(attribFile, attrib);
				mapper.writeValue(tabelaFile, tables);

				File f = new File(tablesFolderPath + currentTable.getId() + ".json");
				f.createNewFile();
				Files.write(f.toPath(), Arrays.asList("{\"records\": []}"), Charset.forName("UTF-8"));

			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void acao33(Token token) throws SemanticError {
		try {

			ObjectMapper mapper = new ObjectMapper();
			File attribFile = new File(metaFolderPath + "atributo.json");
			Attributes attrib = mapper.readValue(attribFile, Attributes.class);

			List<String> tAux = new ArrayList<String>();
			for (Table t : tabelasSelect) {
				tAux.add(t.getId());
			}

			List<Attribute> atributosTabelas = attrib.getAttributes().stream()
					.filter(a -> tAux.contains(a.getIdTabela())).collect(Collectors.toList());

			for (Condicao c : condicoesSelect) {
				if (c.campo1 != null) {
					if (c.campo1.getIdTabela() == null) {
						int aux = 0;
						for (Attribute b : atributosTabelas) {
							if (b.getId().equals(c.campo1.getId())) {
								if (aux == 1) {
									throw new SemanticError("atributo " + c.campo1.getId() + " ambiguo",
											token.getPosition());
								}
								c.campo1.setIdTabela(b.getIdTabela());
								aux++;
							}
						}

						if (aux == 0) {
							throw new SemanticError("atributo " + c.campo1.getId() + " nao encontrado",
									token.getPosition());
						}
					} else {
						boolean aux = false;
						for (Attribute b : atributosTabelas) {
							if (b.getIdTabela().equals(c.campo1.getIdTabela())) {
								if (b.getId().equals(c.campo1.getId())) {
									aux = true;
								}
							}
						}

						if (!aux) {
							throw new SemanticError(
									"atributo " + c.campo1.getIdTabela() + "." + c.campo1.getId() + " nao encontrado",
									token.getPosition());
						}
					}
				}

				if (c.campo2 != null) {
					if (c.campo2.getIdTabela() == null) {
						int aux = 0;
						for (Attribute b : atributosTabelas) {
							if (b.getId().equals(c.campo2.getId())) {
								if (aux == 1) {
									throw new SemanticError("atributo " + c.campo2.getId() + " ambiguo",
											token.getPosition());
								}
								c.campo2.setIdTabela(b.getIdTabela());
								aux++;
							}
						}

						if (aux == 0) {
							throw new SemanticError("atributo " + c.campo2.getId() + " nao encontrado",
									token.getPosition());
						}
					} else {
						boolean aux = false;
						for (Attribute b : atributosTabelas) {
							if (b.getIdTabela().equals(c.campo2.getIdTabela())) {
								if (b.getId().equals(c.campo2.getId())) {
									aux = true;
								}
							}
						}

						if (!aux) {
							throw new SemanticError(
									"atributo " + c.campo2.getIdTabela() + "." + c.campo2.getId() + " nao encontrado",
									token.getPosition());
						}
					}
				}
			}

		} catch (IOException e) {

		}

		/*
		 * Object last = stack.pop();
		 * 
		 * List<Condicao> condicoes = new ArrayList<Condicao>();
		 * List<TableField> campos = new ArrayList<TableField>();
		 * 
		 * TableField tf = null;
		 * 
		 * while (!(last instanceof Table)) { if (last instanceof Condicao) {
		 * Condicao c = ((Condicao) last); c.campo2 = tf; condicoes.add(c); }
		 * else { campos.add((TableField) last); } last = stack.pop(); }
		 * stack.push(last); stack.push(campos); stack.push(condicoes);
		 */
	}

	class Condicao {
		public String operador;
		public Attribute campo1;
		public Attribute campo2;
		public Object valor1;
		public Object valor2;
		public boolean condicaoNull;
		public boolean isNull;
	}

	private void acao32(Token token) {
		// TODO Auto-generated method stub

	}

	class TableField {
		Object atributo;
		Object tabela;

		TableField(Object v1, Object v2) {
			this.atributo = v1;
			this.tabela = v2;
		}
	}

	private void acao31(Token token) throws SemanticError {
		/*
		 * currentAction = ACTIONS.SELECT; List<TableField> tabelaAtributo = new
		 * ArrayList<TableField>();
		 * 
		 * do { Object last = stack.pop(); String atributo = (String) last;
		 * String tabela = (String) stack.pop();
		 * 
		 * TableField tuple = new TableField(atributo, tabela);
		 * tabelaAtributo.add(tuple);
		 * 
		 * } while (!stack.isEmpty());
		 * 
		 * for (TableField tuple : tabelaAtributo) { stack.push(tuple); }
		 * 
		 * ObjectMapper mapper = new ObjectMapper(); File tabelaFile = new
		 * File(metaFolderPath + "atributo.json"); Attributes at =
		 * mapper.readValue(tabelaFile, Attributes.class);
		 * 
		 * for (TableField t : tabelaAtributo) { String atributo = (String)
		 * t.atributo; String tabela = (String) t.tabela;
		 * 
		 * boolean found = false; for (Attribute a : at.getAttributes()) { if
		 * (atributo.equals(a.getId()) && tabela.equals(a.getIdTabela())) {
		 * found = true; } }
		 * 
		 * if (!found) { throw new SemanticError("atributo " + atributo +
		 * " nao encontrado na tabela " + tabela, token.getPosition()); } }
		 */

		// Tables tables = mapper.readValue(tabelaFile, Tables.class);
		// Table tableMeta = tables.getTables().stream().filter(ta ->
		// tableId.equals(ta.getId())).findFirst().get();

	}

	private void acao30(Token token) {
		if (condicaoAtual == null) {
			Condicao c = new Condicao();
			c.valor1 = token.getLexeme();
			condicaoAtual = c;
		} else {
			condicaoAtual.valor2 = token.getLexeme();

			condicoesSelect.add(condicaoAtual);
			condicaoAtual = null;
		}
	}

	private void acao29(Token token) {
		// TODO Auto-generated method stub

	}

	private void acao28(Token token) {
		// TODO Auto-generated method stub

	}

	private void acao27(Token token) {
		// TODO Auto-generated method stub

	}

	private void acao26(Token token) {
		// TODO Auto-generated method stub

	}

	private void acao25(Token token) {
		// TODO Auto-generated method stub

	}

	private void acao24(Token token) throws SemanticError {
		File f = new File(tablesFolderPath + token.getLexeme() + ".json");
		if (!f.exists()) {
			throw new SemanticError("tabela " + token.getLexeme() + " inexistente", token.getPosition());
		}

		Table t = new Table();
		t.setId(token.getLexeme());

		tabelasSelect.add(t);

		/*
		 * List<TableField> tfs = new ArrayList<TableField>(); List<Table>
		 * tables = new ArrayList<Table>();
		 * 
		 * while (!stack.isEmpty()) { Object last = stack.pop();
		 * 
		 * if (last instanceof TableField) { tfs.add((TableField) last); } else
		 * { tables.add((Table) last); } }
		 * 
		 * for (Table t : tables) { stack.push(t); }
		 * 
		 * Table t = new Table(); t.setId(token.getLexeme()); stack.push(t);
		 * 
		 * for (TableField tf : tfs) { stack.push(tf); }
		 */

	}

	private void acao23(Token token) {
		// TODO Auto-generated method stub

	}

	private void acao22(Token token) {
		// TODO Auto-generated method stub

	}

	private void acao21(Token token) {
		String tabela = (String) stack.pop();

		Attribute a = new Attribute();
		a.setIdTabela(tabela);
		a.setId(token.getLexeme());

		if (condicaoAtual == null) {
			Condicao c = new Condicao();
			c.campo1 = a;
			condicaoAtual = c;
		} else {
			condicaoAtual.campo2 = a;

			condicoesSelect.add(condicaoAtual);
			condicaoAtual = null;
		}
	}

	private void acao20(Token token) throws SemanticError {
		if (ordemColunasInsert.isEmpty()) {

			if (atributoAtual >= atributosInsert.size()) {
				throw new SemanticError("foram informados mais valores do que a quantidade de colunas",
						token.getPosition());
			}

			Attribute atribAtual = atributosInsert.get(atributoAtual);

			if (token.getId() == 3) { // numero
				if ('I' != atribAtual.getType()) {
					throw new SemanticError("tipo invalido atributo " + atribAtual.getId(), token.getPosition());
				}
			} else if (token.getId() == 4) {// literal
				if ('I' == atribAtual.getType()) {
					throw new SemanticError("tipo invalido atributo " + atribAtual.getId(), token.getPosition());
				}
			}

			if (token.getId() != 3 && token.getId() != 4) {
				if (atribAtual.isNotNull()) {
					throw new SemanticError("atributo " + atribAtual.getId() + " nao pode ser nulo",
							token.getPosition());
				}
			}

		} else {

			if (atributoAtual >= ordemColunasInsert.size()) {
				throw new SemanticError("foram informados mais valores do que colunas durante o insert",
						token.getPosition());
			}

			Attribute atribAtual = ordemColunasInsert.get(atributoAtual);

			if (token.getId() == 3) { // numero
				if ('I' != atribAtual.getType()) {
					throw new SemanticError("tipo invalido atributo " + atribAtual.getId(), token.getPosition());
				}
			} else if (token.getId() == 4) {// literal
				if ('I' == atribAtual.getType()) {
					throw new SemanticError("tipo invalido atributo " + atribAtual.getId(), token.getPosition());
				}
			}

			if (token.getId() != 3 && token.getId() != 4) {
				if (atribAtual.isNotNull()) {
					throw new SemanticError("atributo " + atribAtual.getId() + " nao pode ser nulo",
							token.getPosition());
				}
			}
		}

		valoresInsert.add(token.getLexeme());
		atributoAtual++;
	}

	private void acao19(Token token) throws SemanticError {

		try {

			// daqui em diante eh soh pra ver se existe na tabela o atributo q o
			// cara informou
			ObjectMapper mapper = new ObjectMapper();

			String tableId = (String) insertInto.getId();

			File attributeFile = new File(metaFolderPath + "atributo.json");
			Attributes attributes;

			attributes = mapper.readValue(attributeFile, Attributes.class);

			List<Attribute> attributesMeta = attributes.getAttributes().stream()
					.filter(a -> tableId.equals(a.getIdTabela())).collect(Collectors.toList());

			atributosInsert = attributesMeta;

			Collections.sort(atributosInsert, new Comparator<Attribute>() {
				@Override
				public int compare(Attribute at1, Attribute at2) {
					return at1.getSeq().compareTo(at2.getSeq());
				}
			});

			for (Attribute attributeId : ordemColunasInsert) {

				boolean found = false;
				for (Attribute i : attributesMeta) {
					if (attributeId.getId().equals(i.getId())) {
						attributeId.setDef(i.getDef());
						attributeId.setDesc(i.getDesc());
						attributeId.setForeignKey(i.isForeignKey());
						attributeId.setIdTabela(i.getIdTabela());
						attributeId.setNotNull(i.isNotNull());
						attributeId.setPrimaryKey(i.isPrimaryKey());
						attributeId.setSeq(i.getSeq());
						attributeId.setSize(i.getSize());
						attributeId.setType(i.getType());

						found = true;
					}
				}

				if (!found) {
					throw new SemanticError("atributo " + attributeId + " inexistente", token.getPosition());
				}
			}

		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void acao18(Token token) {
		condicaoAtual.operador = token.getLexeme();
	}

	private void acao17(Token token) {
		// TODO Auto-generated method stub
	}

	/**
	 * Seta o banco de dados ativo para o informado pelo usuario
	 * 
	 * @param token
	 * @throws SemanticError
	 *             caso o databse nao existir
	 */
	private void acao16(Token token) throws SemanticError {
		boolean IS_LINUX = "Linux".equals(System.getProperty("os.name"));
		String USER = System.getProperty("user.name");

		String DATABASES_FOLDER_PATH;
		if (IS_LINUX) {
			DATABASES_FOLDER_PATH = "/home/" + USER + "/purpledb/";
		} else {
			DATABASES_FOLDER_PATH = "C:\\Users\\" + USER + "\\purpledb\\";
		}

		File f = new File(DATABASES_FOLDER_PATH + token.getLexeme());
		if (!f.exists()) {
			throw new SemanticError("database " + token.getLexeme() + " nao existe ", token.getPosition());
		}

		selectedDB = DATABASES_FOLDER_PATH + token.getLexeme();

		tablesFolderPath = selectedDB + dirSep + "tables" + dirSep;
		metaFolderPath = selectedDB + dirSep + "tables" + dirSep + "meta" + dirSep;

	}

	/**
	 * Descreve a tabela, incluindo nome, atributos e indices.
	 * 
	 * @param token
	 * @throws SemanticError
	 */
	private void acao15(Token token) throws SemanticError {
		try {
			String table = token.getLexeme();
			StringBuilder sb = new StringBuilder();
			sb.append("Tabela: ").append(table).append("/n");
			File atributesFile = new File(metaFolderPath + "atributo.json");
			ObjectMapper mapper = new ObjectMapper();
			Attributes atributes;
			atributes = mapper.readValue(atributesFile, Attributes.class);

			for (Attribute atribute : atributes.getAttributes()) {
				if (atribute.getIdTabela().equalsIgnoreCase(table)) {
					sb.append("Atributo: ").append(atribute.getId()).append(", Type: ").append(atribute.getType())
							.append(", Size: ").append(atribute.getSize()).append(", PK: ")
							.append(atribute.isPrimaryKey()).append(", FK: ").append(atribute.isForeignKey())
							.append("\n");

				}
			}

			File indexFile = new File(metaFolderPath + "indices.json");
			Indices indices = mapper.readValue(indexFile, Indices.class);
			for (Index index : indices.getIndices()) {
				if (index.getTableId().equalsIgnoreCase(table)) {
					sb.append("Index: ").append(index.getId()).append(", Attributes: ");
					for (Attribute atrib : index.getAttributes()) {
						sb.append(atrib.getId()).append(", ");
					}

				}
			}

			// System.out.println(sb.toString());
			// TODO - Rever
			throw new SemanticError(sb.toString(), 0);

		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void acao14(Token token) {
		currentAction = ACTIONS.DROP_INDEX;
		stack.push(token.getLexeme());
	}

	private void acao13(Token token) {
		currentAction = ACTIONS.DROP_TABLE;
		stack.push(token.getLexeme());
	}

	private void acao12(Token token) {
		String attribId = (String) stack.pop();

		Attribute attrib = new Attribute();
		attrib.setId(attribId);
		attrib.setType('C');
		attrib.setSize(Integer.valueOf(token.getLexeme()));

		stack.push(attrib);

	}

	private void acao10(Token token) {
		String attribId = (String) stack.pop();

		Attribute attrib = new Attribute();
		attrib.setId(attribId);
		attrib.setType('I');

		stack.push(attrib);
	}

	private void acao9(Token token) throws SemanticError {
		String campoReferenciado = token.getLexeme();
		String tabelaReferenciada = (String) stack.pop();
		Attribute campoFK = (Attribute) stack.pop();
		Table table = createTable;

		Attribute atribute = getAtribute(campoReferenciado, tabelaReferenciada);
		if (atribute == null) {
			throw new SemanticError("atributo referenciado" + atribute + " nao existe", token.getPosition());
		}

		campoFK.setForeignKey(true);
		Restriction restriction = new Restriction();
		restriction.attributeTo = campoReferenciado;
		restriction.tableTo = tabelaReferenciada;
		restriction.attributeFrom = campoFK.getId();
		restriction.tableFrom = table.getId();

		restrictions.add(restriction);

		stack.push(campoFK);
	}

	private void acao7(Token token) throws SemanticError {
		List<Attribute> atributos = new ArrayList<Attribute>();
		String fk = token.getLexeme();
		Attribute atributoEncontrado = null;

		while (!stack.isEmpty()) {
			Object last = stack.pop();

			atributos.add((Attribute) last);
		}

		for (Attribute atrib : atributos) {
			if (atrib.getId().equalsIgnoreCase(fk)) {
				if (atrib.isPrimaryKey()) {
					throw new SemanticError("atributo " + fk + " eh uma PK", token.getPosition());
				}

				atributoEncontrado = atrib;
			} else {
				stack.push(atrib);
			}
		}

		// garante que o atributo esteja no topo da pilha
		if (atributoEncontrado != null) {
			stack.push(atributoEncontrado);
		} else {
			throw new SemanticError("atributo " + fk + " inexistente", token.getPosition());
		}
	}

	private void acao6(Token token) throws SemanticError {

		boolean isPrimaryKey;

		Object last = stack.pop();

		if (last instanceof Attribute) {
			currentAttributes = new ArrayList<Attribute>();

			while (last instanceof Attribute) {
				currentAttributes.add((Attribute) last);

				last = stack.pop();
			}

			if (!(last instanceof Table)) {
				throw new SemanticError("era esperado uma tabela", token.getPosition());
			}

			// ((Table) last).setAttributes(currentAttributes);

			stack.push(last);
		} else {
			boolean hasAttribute = false;
			while (!(last instanceof Table)) {
				for (Attribute a : currentAttributes) {
					if (((String) last).equals(a.getId())) {
						a.setPrimaryKey(true);
						hasAttribute = true;
					}
				}

				if (!hasAttribute) {
					throw new SemanticError("atributo " + (String) last + " inexistente", token.getPosition());
				}

				hasAttribute = true;

				last = stack.pop();
			}

			stack.push(last);
		}

	}

	private void acao5(Token token) {
		stack.push(token.getLexeme());
	}

	/**
	 * Verifica se a tabela existe e se n�o existe nenhum indice criado para
	 * esta tabela com o mesmo nome, caso sim desempilha o objeto {@link Index}
	 * e seta o nome da tabela caso n�o exista lan�a uma exce��o.
	 * 
	 * @param token
	 *            nome da tabela
	 * @throws SemanticError
	 *             caso a tabela n�o exista ou j� exista um indice com o
	 *             mesmo nome nessa tabela
	 */
	private void acao4(Token token) throws SemanticError {
		try {
			File f = new File(tablesFolderPath + token.getLexeme() + ".json");
			if (!f.exists()) {
				throw new SemanticError("tabela dependente n�o existe", token.getPosition());
			}

			Index index = (Index) stack.pop();
			index.setTableId(token.getLexeme());

			File indexFile = new File(metaFolderPath + "indice.json");
			ObjectMapper mapper = new ObjectMapper();
			Indices indices;
			indices = mapper.readValue(indexFile, Indices.class);

			for (Index existentIndex : indices.getIndices()) {
				if (existentIndex.getId().equalsIgnoreCase(index.getId())
						&& existentIndex.getTableId().equalsIgnoreCase(index.getTableId())) {
					throw new SemanticError(
							"J� existe um indice com o nome: " + index.getId() + " na tabela: " + index.getTableId(),
							token.getPosition());
				}

			}

			stack.push(index);

		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void acao8(Token token) throws SemanticError {
		File f = new File(tablesFolderPath + token.getLexeme() + ".json");
		if (!f.exists()) {
			throw new SemanticError("tabela dependente nao existe", token.getPosition());
		}
		// empilha o nome da tabela
		stack.push(token.getLexeme());
	}

	/**
	 * Define a a��o atual como sendo a cria��o do index, cria um novo
	 * Objeto {@link Index} com o nome do indice e adiciona na pilha
	 * 
	 * @param token
	 *            nome do indice
	 */
	private void acao3(Token token) {
		currentAction = ACTIONS.CREATE_INDEX;
		Index index = new Index();
		index.setId(token.getLexeme());
		stack.push(index);
	}

	private void acao2(Token token) {
		createTable = new Table(token.getLexeme());
	}

	// ACAO 0 AINDA VAI EXISTIR, VAI SER RESPONSAVEL POR LIMPAR A STACK E AS
	// VARIAVEIS
	private void acao0(Token token) throws SemanticError {
		createTable = null;

		insertInto = null;
		ordemColunasInsert = new ArrayList<Attribute>();
		valoresInsert = new ArrayList<String>();
		atributosInsert = new ArrayList<Attribute>();
		atributoAtual = 0;

		selecionarTudo = false;

		atributosSelect = new ArrayList<Attribute>();
		tabelasSelect = new ArrayList<Table>();
		condicoesSelect = new ArrayList<Condicao>();
		condicaoAtual = null;

		stack = new Stack();

		/*
		 * switch (currentAction) { case CREATE_TABLE: {
		 * 
		 * break; } case DROP_TABLE: { ObjectMapper mapper = new ObjectMapper();
		 * String table = (String) stack.pop(); File f = new
		 * File(tablesFolderPath + table + ".json"); f.delete(); File tabelaFile
		 * = new File(metaFolderPath + "tabela.json"); Tables tables =
		 * mapper.readValue(tabelaFile, Tables.class); List<Table> tablesList =
		 * tables.getTables(); int removeIndex = -1; // Procura a tabela
		 * correspondente e remove do metadata for (int i = 0; i <
		 * tablesList.size(); i++) { if
		 * (tablesList.get(i).getId().equalsIgnoreCase(table)) { removeIndex =
		 * i; break; } } if (removeIndex != -1) {
		 * tablesList.remove(removeIndex); } tables.setTables(tablesList);
		 * 
		 * // Remove os indices existentes sobre a tabela File indexFile = new
		 * File(metaFolderPath + "indices.json"); Indices indices =
		 * mapper.readValue(indexFile, Indices.class);
		 * 
		 * List<Index> indexToRemove = new ArrayList<>(); List<Index>
		 * currentIndices = indices.getIndices(); for (Index index :
		 * currentIndices) { if (index.getTableId().equalsIgnoreCase(table)) {
		 * indexToRemove.add(index); } }
		 * currentIndices.removeAll(indexToRemove);
		 * indices.setIndices(currentIndices);
		 * 
		 * // Verifica se existe algum campo fk apontando para esta tabela File
		 * restricaoFile = new File(metaFolderPath + "restricao.json");
		 * Restrictions restrictions = mapper.readValue(restricaoFile,
		 * Restrictions.class);
		 * 
		 * for (Restriction fk : restrictions.getRestrictions()) { if
		 * (fk.tableTo.equalsIgnoreCase(table)) { throw new SemanticError(
		 * "a tabela n�o pode ser removida pois existe uma FK na tabela " +
		 * fk.tableFrom + " campo " + fk.attributeFrom +
		 * " fazendo referencia a esta tabela no campo " + fk.attributeTo,
		 * token.getPosition()); } }
		 * 
		 * mapper.writeValue(indexFile, indices); mapper.writeValue(tabelaFile,
		 * tables);
		 * 
		 * break; } case DROP_INDEX: { ObjectMapper mapper = new ObjectMapper();
		 * String indexName = (String) stack.pop(); // Remove os indices
		 * existentes sobre a tabela File indexFile = new File(metaFolderPath +
		 * "indices.json"); Indices indices = mapper.readValue(indexFile,
		 * Indices.class);
		 * 
		 * List<Index> indexToRemove = new ArrayList<>(); List<Index>
		 * currentIndices = indices.getIndices(); for (Index index :
		 * currentIndices) { if (index.getId().equalsIgnoreCase(indexName)) {
		 * indexToRemove.add(index); } }
		 * currentIndices.removeAll(indexToRemove);
		 * indices.setIndices(currentIndices);
		 * 
		 * mapper.writeValue(indexFile, indices); break; } case INSERT: {
		 * ObjectMapper mapper = new ObjectMapper();
		 * 
		 * // desempilha o ultimo valor, tabela Table t = (Table) stack.pop();
		 * 
		 * HashMap values = new HashMap(); // para cada atributo passado e seu
		 * valor, adiciona em um map while (!cAttributes.isEmpty()) { Object
		 * valor = cAttributes.pop(); Object id = cAttributes.pop();
		 * values.put(id, valor); }
		 * 
		 * // carrega a tabela String tabelaPath = tablesFolderPath + t.getId()
		 * + ".json";
		 * 
		 * Tabela tabela = mapper.readValue(new File(tabelaPath), Tabela.class);
		 * // salva o novo valor tabela.addRecord(values);
		 * 
		 * mapper.writeValue(new File(tabelaPath), tabela);
		 * 
		 * break; } case SELECT: { ObjectMapper mapper = new ObjectMapper();
		 * List<Condicao> condicoes = (List<Condicao>) stack.pop(); List<Table>
		 * tabelas = new ArrayList<Table>(); List<TableField> campos =
		 * (List<TableField>) stack.pop();
		 * 
		 * do { Object last = stack.pop(); if (last instanceof Table) {
		 * tabelas.add((Table) last); }
		 * 
		 * } while (!stack.isEmpty());
		 * 
		 * HashMap<String, List<HashMap>> tables = new HashMap(); for (Table t :
		 * tabelas) { Tabela ta = mapper.readValue(new File(tablesFolderPath +
		 * t.getId() + ".json"), Tabela.class); tables.put(t.getId(),
		 * ta.getRecords()); }
		 * 
		 * List<HashMap> retorno = new ArrayList<HashMap>(); for (Condicao c :
		 * condicoes) { TableField c1 = c.campo1; TableField c2 = c.campo2;
		 * Object v = c.valor2;
		 * 
		 * if ("=".equals(c.operador)) { tables.put((String) c1.tabela,
		 * tables.get(c1.tabela).stream() .filter(g -> ((String)
		 * g.get(c1.atributo)).compareTo(getValue(c2, v, g)) == 0)
		 * .collect(Collectors.toList())); } else if (">".equals(c.operador)) {
		 * tables.put((String) c1.tabela, tables.get(c1.tabela).stream()
		 * .filter(g -> ((String) g.get(c1.atributo)).compareTo(getValue(c2, v,
		 * g)) > 0) .collect(Collectors.toList())); } else if
		 * (">=".equals(c.operador)) { tables.put((String) c1.tabela,
		 * tables.get(c1.tabela).stream() .filter(g -> ((String)
		 * g.get(c1.atributo)).compareTo(getValue(c2, v, g)) == 0 || ((String)
		 * g.get(c1.atributo)).compareTo(getValue(c2, v, g)) > 0)
		 * .collect(Collectors.toList())); } else if ("<".equals(c.operador)) {
		 * tables.put((String) c1.tabela, tables.get(c1.tabela).stream()
		 * .filter(g -> ((String) g.get(c1.atributo)).compareTo(getValue(c2, v,
		 * g)) < 0) .collect(Collectors.toList())); } else if
		 * ("<=".equals(c.operador)) { tables.put((String) c1.tabela,
		 * tables.get(c1.tabela).stream() .filter(g -> ((String)
		 * g.get(c1.atributo)).compareTo(getValue(c2, v, g)) == 0 || ((String)
		 * g.get(c1.atributo)).compareTo(getValue(c2, v, g)) < 0)
		 * .collect(Collectors.toList())); } else if ("<>".equals(c.operador)) {
		 * tables.put((String) c1.tabela, tables.get(c1.tabela).stream()
		 * .filter(g -> ((String) g.get(c1.atributo)).compareTo(getValue(c2, v,
		 * g)) != 0) .collect(Collectors.toList())); } }
		 * 
		 * List<HashMap> output = new ArrayList<HashMap>();
		 * 
		 * for (Entry<String, List<HashMap>> set : tables.entrySet()) {
		 * 
		 * for (Map h : set.getValue()) { Map row = new HashMap(); for
		 * (TableField campo : campos) { if (campo.tabela.equals(set.getKey()))
		 * { row.put(campo.atributo, h.get(campo.atributo)); } }
		 * output.add((HashMap) row); }
		 * 
		 * }
		 * 
		 * // System.out.println(output);
		 * 
		 * // Exibindo o select para o usu�rio (TODO - Rever) throw new
		 * SemanticError(output.toString(), 0);
		 * 
		 * } case CREATE_INDEX:
		 * 
		 * /* Desempilha os campos do indice e seta para o objeto index
		 * 
		 * ObjectMapper mapper = new ObjectMapper(); List<Attribute>
		 * attributeList = new ArrayList<>(); Object pop = stack.pop(); while
		 * (!(pop instanceof Index)) { Attribute attribute = new Attribute();
		 * attribute.setId((String) pop); attributeList.add(attribute); pop =
		 * stack.pop(); }
		 * 
		 * /* Lan�a exce��o caso o usuario n�o tenha definido os campos
		 * 
		 * if (attributeList.isEmpty()) { throw new SemanticError(
		 * "n�o foram definidos os campos do indice", token.getPosition()); }
		 * 
		 * Index index = (Index) pop; index.setAttributes(attributeList);
		 * 
		 * /* Verifica se os campos existem na tabela
		 * 
		 * for (Attribute attribute : attributeList) { Attribute atribute =
		 * getAtribute(attribute.getId(), index.getTableId()); if (atribute ==
		 * null) { throw new SemanticError( "campo: " + attribute.getId() +
		 * "n�o existe na tabela: " + index.getTableId(),
		 * token.getPosition()); } }
		 * 
		 * File indexFile = new File(metaFolderPath + "indices.json"); Indices
		 * indices = mapper.readValue(indexFile, Indices.class);
		 * 
		 * indices.getIndices().add(index); mapper.writeValue(indexFile,
		 * indices);
		 * 
		 * break; }
		 * 
		 * stack.clear(); // currentAttributes.clear(); insert.clear();
		 * currentAction = null;
		 */

	}

	public String getValue(Attribute c2, Object v, Map g) {
		return c2 == null ? (String) v : (String) g.get(c2.getId());
	}

	private void acao1(Token token) {
		File databasesFolder = new File(Window.DATABASES_FOLDER_PATH);
		if (!databasesFolder.exists()) {
			databasesFolder.mkdir();
		}

		String databaseFolderPath = Window.DATABASES_FOLDER_PATH + token.getLexeme() + dirSep;
		File databaseFolder = new File(databaseFolderPath);
		databaseFolder.mkdir();

		tablesFolderPath = databaseFolderPath + "tables" + dirSep;
		new File(tablesFolderPath).mkdir();
		String metaFolder = tablesFolderPath + "meta" + dirSep;
		new File(metaFolder).mkdir();

		try {
			File f = new File(metaFolder + "tabela.json");
			f.createNewFile();
			Files.write(f.toPath(), Arrays.asList("{\"tables\": []}"), Charset.forName("UTF-8"));
			f = new File(metaFolder + "atributo.json");
			f.createNewFile();
			Files.write(f.toPath(), Arrays.asList("{\"attributes\": []}"), Charset.forName("UTF-8"));
			f = new File(metaFolder + "atributoIndice.json");
			f.createNewFile();
			Files.write(f.toPath(), Arrays.asList("{\"attributeIndexes\": []}"), Charset.forName("UTF-8"));
			f = new File(metaFolder + "indice.json");
			f.createNewFile();
			Files.write(f.toPath(), Arrays.asList("{\"indexes\": []}"), Charset.forName("UTF-8"));
			f = new File(metaFolder + "atributoRestricao.json");
			f.createNewFile();
			Files.write(f.toPath(), Arrays.asList("{\"attributeRestrictions\": []}"), Charset.forName("UTF-8"));
			f = new File(metaFolder + "restricao.json");
			f.createNewFile();
			Files.write(f.toPath(), Arrays.asList("{\"restrictions\": []}"), Charset.forName("UTF-8"));
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}

		boolean IS_LINUX = "Linux".equals(System.getProperty("os.name"));
		String USER = System.getProperty("user.name");
		String DATABASES_FOLDER_PATH;
		if (IS_LINUX) {
			DATABASES_FOLDER_PATH = "/home/" + USER + "/purpledb/";
		} else {
			DATABASES_FOLDER_PATH = "C:\\Users\\" + USER + "\\purpledb\\";
		}
		selectedDB = DATABASES_FOLDER_PATH + token.getLexeme();

		tablesFolderPath = selectedDB + dirSep + "tables" + dirSep;
		metaFolderPath = selectedDB + dirSep + "tables" + dirSep + "meta" + dirSep;
		
		observer.onActionExec(1);
	}

	public Attribute getAtribute(String attributeId, String atributeTableID) {
		try {
			File atributesFile = new File(metaFolderPath + "atributo.json");
			ObjectMapper mapper = new ObjectMapper();
			Attributes atributes;
			atributes = mapper.readValue(atributesFile, Attributes.class);

			for (Attribute atribute : atributes.getAttributes()) {
				if (atribute.getId().equalsIgnoreCase(attributeId)
						&& atribute.getIdTabela().equalsIgnoreCase(atributeTableID)) {
					return atribute;

				}
			}
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

}
