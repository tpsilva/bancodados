package gals;

public class LexicalError extends AnalysisError
{
	public LexicalError(String msg, int startPosition, int endPosition) {
		super(msg, startPosition, endPosition);
	}

	public LexicalError(String msg) {
		super(msg);
	}

	public String getSimbol(String text) {
		if (getEndPosition() > -1 && getEndPosition() > getStartPosition()) {
			return text.substring(getStartPosition(), getEndPosition());
		} else {
			return text.substring(getStartPosition(), getStartPosition() + 1);
		}
	}
}
