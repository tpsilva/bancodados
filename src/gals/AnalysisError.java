package gals;

public class AnalysisError extends Exception
{
	
	private int startPosition;
	private int endPosition;

	public AnalysisError(String msg, int startPosition, int endPosition) {
		super(msg);
		this.startPosition = startPosition;
		this.endPosition = endPosition;
	}
	
	public AnalysisError(String msg, int position){
		super(msg);
		this.startPosition = position;
	}

	public AnalysisError(String msg) {
		super(msg);
		this.startPosition = -1;
		this.endPosition = -1;
	}

	public int getStartPosition() {
		return startPosition;
	}

	public int getEndPosition() {
		return endPosition;
	}

}
