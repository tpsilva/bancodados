package gals;

public class SyntaticError extends AnalysisError {
	private String lexemeFound;

	public SyntaticError(String msg, int startPosition, int endPosition, String lexemeFound) {
		super(msg, startPosition, endPosition);

		this.lexemeFound = lexemeFound;
	}

	public SyntaticError(String msg, int position) {
		super(msg, position);
	}

	public SyntaticError(String msg) {
		super(msg);
	}

	public String getSimbol(String text) {
		if (getEndPosition() > -1 && getEndPosition() > getStartPosition()) {
			return text.substring(getStartPosition(), getEndPosition());
		} else {
			return text.substring(getStartPosition(), getStartPosition() + 1);
		}
	}

	public String getLexemeFound() {
		return lexemeFound;
	}
}
