package models;

import java.util.List;

public class Restrictions {

	private List<Restriction> restrictions;

	public List<Restriction> getRestrictions() {
		return restrictions;
	}

	public void setRestrictions(List<Restriction> restrictions) {
		this.restrictions = restrictions;
	}

	public void addRestriction(Restriction restriction) {
		restrictions.add(restriction);
	}

}
