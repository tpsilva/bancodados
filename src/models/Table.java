package models;

import java.util.List;

public class Table {

	private String id;

	public Table(){
		
	}
	
	public Table(String id){
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
