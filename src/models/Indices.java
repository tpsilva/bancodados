package models;

import java.util.List;

public class Indices {

	private List<Index> indices;

	public List<Index> getIndices() {
		return indices;
	}

	public void setIndices(List<Index> indices) {
		this.indices = indices;
	}

	public void addIndex(Index indices) {
		this.indices.add(indices);
	}

}
