package models;

import java.util.List;

public class Attribute {

	private String id;
	private String idTabela;
	private Integer seq;
	private char type;
	private Integer size;
	private Integer desc;
	private String def;
	private boolean notNull;
	private boolean primaryKey;
	private boolean foreignKey;

	public String getIdTabela() {
		return idTabela;
	}

	public boolean isNotNull() {
		return notNull;
	}


	public void setNotNull(boolean notNull) {
		this.notNull = notNull;
	}


	public void setIdTabela(String idTabela) {
		this.idTabela = idTabela;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getSeq() {
		return seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}

	public char getType() {
		return type;
	}

	public void setType(char type) {
		this.type = type;
	}

	public Integer getSize() {
		return size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}

	public Integer getDesc() {
		return desc;
	}

	public void setDesc(Integer desc) {
		this.desc = desc;
	}

	public String getDef() {
		return def;
	}

	public void setDef(String def) {
		this.def = def;
	}

	public boolean isPrimaryKey() {
		return primaryKey;
	}

	public void setPrimaryKey(boolean primaryKey) {
		this.primaryKey = primaryKey;
	}

	public boolean isForeignKey() {
		return foreignKey;
	}

	public void setForeignKey(boolean foreignKey) {
		this.foreignKey = foreignKey;
	}

}
