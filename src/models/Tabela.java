package models;

import java.util.HashMap;
import java.util.List;

public class Tabela {

	private List<HashMap> records;

	public List<HashMap> getRecords() {
		return records;
	}

	public void setRecords(List<HashMap> records) {
		this.records = records;
	}
	
	public void addRecord(HashMap record){
		records.add(record);
	}
	
}
