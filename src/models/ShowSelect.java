package models;

import java.util.List;

public class ShowSelect extends Exception{

	private List rows;
	
	public ShowSelect(List rows){
		this.rows = rows;
	}
	
	public List getRows(){
		return rows;
	}
	
}
