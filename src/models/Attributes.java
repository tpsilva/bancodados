package models;

import java.util.List;

public class Attributes {

	private List<Attribute> attributes;

	public List<Attribute> getAttributes() {
		return attributes;
	}

	public void setAttributes(List<Attribute> attributes) {
		this.attributes = attributes;
	}
	
	public void addAttribute(Attribute at){
		this.attributes.add(at);
	}
	
}
