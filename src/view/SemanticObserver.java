package view;

public interface SemanticObserver {

	void onActionExec(Integer action);
	
}
