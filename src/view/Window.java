package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.io.File;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

import gals.LexicalError;
import gals.Lexico;
import gals.SemanticError;
import gals.Semantico;
import gals.Sintatico;
import gals.SyntaticError;
import models.ShowSelect;

public class Window {

	private JFrame frame;
	
	private JPanel mainPanel;
	private JPanel centerPanel;
	
	private PurpleDBToolbar toolbar;
	private PurpleDBEditor editor;
	private PurpleDBMessageBox messageBox;
	
	File databasesFolder;

	static final String NAO_MODIFICADO = "N\u00E3o modificado";
	static final String MODIFICADO = "Modificado";
	
	public static boolean IS_LINUX;
	public static String USER;
	public static String DATABASES_FOLDER_PATH;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Window window = new Window();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Window() {
		IS_LINUX = "Linux".equals(System.getProperty("os.name"));
		USER = System.getProperty("user.name");

		if (IS_LINUX) {
			DATABASES_FOLDER_PATH = "/home/" + USER + "/purpledb/";
		} else {
			DATABASES_FOLDER_PATH = "C:\\Users\\" + USER + "\\purpledb\\";
		}
		
		
		try {
			initialize();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Initialize the contents of the frame.
	 * 
	 * @throws Exception
	 */
	private void initialize() throws Exception {
		databasesFolder = new File(DATABASES_FOLDER_PATH);
		if (!databasesFolder.exists()) {
			databasesFolder.mkdir();
		}
		
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setMinimumSize(new Dimension(900, 610));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		mainPanel = new JPanel();
		frame.getContentPane().add(mainPanel, BorderLayout.CENTER);
		
		mainPanel.setLayout(new BorderLayout(0, 0));

		toolbar = new PurpleDBToolbar();
		toolbar.addButtonActionListener(PurpleDBToolbar.BUTTONS.RUN, l -> compilarClick(l));
		mainPanel.add(toolbar, BorderLayout.NORTH);
		
		centerPanel = new JPanel();
		centerPanel.setBorder(new LineBorder(new Color(0, 0, 0)));
		mainPanel.add(centerPanel, BorderLayout.CENTER);
		centerPanel.setLayout(new BorderLayout(0, 0));
		editor = new PurpleDBEditor();
		
		centerPanel.add(editor, BorderLayout.CENTER);
		
		messageBox = new PurpleDBMessageBox();
		centerPanel.add(messageBox, BorderLayout.SOUTH);

	}
	
	private void compilarClick(ActionEvent actionEvent) {
		try {
			
			Lexico lexico = new Lexico();
			lexico.setInput(editor.getText());
			
			
			Sintatico sintatico = new Sintatico();
			
			Semantico semantico = new Semantico();
			
			sintatico.parse(lexico, semantico);

			
			messageBox.setText("comando executado com sucesso");
		} catch (LexicalError e) {
			if (e.getMessage().contains("s�mbolo")) {
				messageBox.setText("Erro na linha " + getLine(editor.getText(), e.getStartPosition()) + " - "
						+ e.getSimbol(editor.getText()) + " " + e.getMessage());
			} else {
				messageBox.setText(
						"Erro na linha " + getLine(editor.getText(), e.getStartPosition()) + " - " + e.getMessage());
			}
		} catch (SyntaticError e) {
			System.out.println(e.getLexemeFound());
			if (e.getMessage().contains("s�mbolo")) {
				
				messageBox.setText("Erro na linha " + getLine(editor.getText(), e.getStartPosition()) + " - Encontrado "
						+ ("$".equalsIgnoreCase(e.getLexemeFound()) ? "fim de programa" : e.getLexemeFound()) + " "
						+ e.getSimbol(editor.getText()) + " " + e.getMessage());
			} else {
				messageBox.setText(
						"Erro na linha " + getLine(editor.getText(), e.getStartPosition()) + " - Encontrado "
								+ ("$".equalsIgnoreCase(e.getLexemeFound()) ? "fim de programa" : e.getLexemeFound()) + " "
							+ e.getMessage());
			}
		} catch (SemanticError e) {
			messageBox.setText(
					"Erro na linha " + getLine(editor.getText(), e.getStartPosition()) + " - "+e.getMessage());
		} catch(ShowSelect e) {
			
			new SelectWindow(e.getRows());
			
		} catch (Exception e){
			System.err.print(e.getMessage());
		}
	}

	public int getLine(String text, int startPosition) {
		String beforeError = text.substring(0, startPosition);
		return beforeError.contains("\n") ? 
				beforeError.endsWith("\n") ? (beforeError.split("\n").length)+1 : (beforeError.split("\n").length) : 1;
	}

}

