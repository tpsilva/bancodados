package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;

public class PurpleDBMessageBox extends JPanel{

	private JScrollPane scrollPane;
	private JTextPane message;
	
	public PurpleDBMessageBox(){
		
		message = new JTextPane();
		message.setFont(new Font("Courier New", Font.PLAIN, 11));
		message.setEditable(false);
		
		
		scrollPane = new JScrollPane(message);
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		scrollPane.setBorder(new LineBorder(new Color(0, 0, 0)));
		scrollPane.setPreferredSize(new Dimension(750, 110));
		scrollPane.setMinimumSize(new Dimension(750, 110));
		
		
		
		setLayout(new BorderLayout(0,0));
		add(scrollPane, BorderLayout.CENTER);
		
	}
	
	public void setText(String text){
		message.setText(text);
	}
	
}
