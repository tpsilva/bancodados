package view;

import java.awt.BorderLayout;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class SelectWindow extends JFrame{
	
	private static final long serialVersionUID = 1L;
	private JTable table;
	private DefaultTableModel dtm;
	private List<HashMap<String, String>> rows;

	public SelectWindow(List<HashMap<String, String>> rows) {
		
		String header[] = new String[]{"none"};
		this.rows = rows;
		
		
		for(HashMap<String, String> row : this.rows){
			Set<Entry<String, String>> set = row.entrySet();
			header = new String[set.size()];
			
			int index = 0;
			
			
			
			for(Entry<String, String> a : set){
				header[index] = a.getKey();
				
				
				index++;
			}
			
			
		}
		
		
//		dtm.addRow(new Object[] { file.getName(), videoInfo.red, videoInfo.green,
	//			videoInfo.blue, videoInfo.variacao, "" });
		
		table = new JTable();
		
		dtm = new DefaultTableModel(0, 0);
		dtm.setColumnIdentifiers(header);
		table.setModel(dtm);
		JScrollPane tableScrollPane = new JScrollPane(table);
		
		
		for(HashMap<String, String> row : this.rows){
			Set<Entry<String, String>> set = row.entrySet();
			
			int index = 0;
			
			Object[] r = new Object[set.size()];
			
			for(Entry<String, String> a : set){
				
				r[index] = a.getValue();
				
				index++;
			}
			
			dtm.addRow(r);
		}
		
		add(tableScrollPane, BorderLayout.CENTER);
		
		
		
		
		pack();
		
		
		
		setVisible(true);
	}
}
