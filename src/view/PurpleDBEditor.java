package view;

import java.awt.BorderLayout;

import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.ScrollPaneConstants;

public class PurpleDBEditor extends JPanel{

	private JScrollPane scrollPane = new JScrollPane();
	private JTextPane editor = new JTextPane();
	private ActionMap editorActions = new ActionMap();
	
	public PurpleDBEditor(){
		
		
		editor.setBorder(new NumberBorder());
		Action[] actionsArray = editor.getActions();
		for (int i = 0; i < actionsArray.length; i++) {
			Action a = actionsArray[i];
			editorActions.put(a.getValue(Action.NAME), a);
		}
		
		scrollPane.setViewportView(editor);
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		
		
		setLayout(new BorderLayout(0,0));
		add(scrollPane, BorderLayout.CENTER);
	}
	
	public String getText(){
		return editor.getText();
	}
	
}
