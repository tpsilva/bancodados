package view;

import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

public class PurpleDBToolbar extends JPanel{
	
	public enum BUTTONS{
		RUN
	}
	
	private JButton run;
	
	PurpleDBToolbar(){

		setLayout(new GridLayout(0,5));
		
		
		run = new JButton("Executar");
		
		add(run);
	}
	
	
	public void addButtonActionListener(BUTTONS button, ActionListener l){
		switch(button){
			case RUN: {
				run.addActionListener(l);
			}
			break;
			default: {
				throw new IllegalArgumentException("Button type not found, check PurpleDBToolbar.BUTTONS");
			}
		}
	}

}
